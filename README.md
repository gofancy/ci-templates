# CI templates

Template files for common Gitlab pipeline jobs in Gradle projects.

The template files are meant to be [included](https://docs.gitlab.com/ee/ci/yaml/#includeremote) in other projects.

Using templates has several benefits: First of all, projects using templates can be set up more quickly and do not have
to maintain their jobs themselves. Secondly, if multiple projects all use the same templates, their pipelines are the
same too, which avoids having to learn and maintain multiple different pipeline configurations.

Additionally, configuring the jobs in a separate project allows the pipeline to be versionised and overall be better
managed. Although we do not have any form of versioning in this repository at the moment, it is already in plans.

See [usage](#usage) for some examples on how to use the provided templates.

**What about Gitlab's standard templates?**

I found that the templates provided by Gitlab do not comply with my ideas of how an optimized pipeline for Gradle
projects should look like. Also, I just like tinkering around and custom templates give me a _much_ greater flexibility
in this regard.

As far as I know, Gitlab also does not offer templates for some jobs I am regularly using (Gitlab pages with Dokka eg).

## Project structure

All pipeline files which are meant to be included are located in the subdirectory `templates`. The top level files are **not**
meant to be included in other projects (mainly `.gitlab-ci.yml` which configures the pipeline for _this_ project).

## Provided templates

Following, a list of all provided job files with a short description. For a more detailed description of what the job
does, it's prerequisites/dependencies and how it works please have a look into the comments added into the file itself.

All files starting with `job_` contain a [hidden CI job](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs). To
enable the job it needs to be extended, see [usage](#usage) for examples.

The project provides following Gradle specific CI files:

- `job_gradle-defaults.yml`: configures the base options of a job to make it work with Gradle, this includes for example
  the `before_script` and `image` blocks
- `job_build.yml`: builds a Gradle project by calling `./gradlew assemble`
- `job_test.yml`: tests a Gradle project by calling `./gradlew check`
- `job_publish.yml`: publishes a Gradle project's artifacts by calling `./gradlew publish`
- `job_pages-dokka.yml`: publishes Gitlab pages with the result of `./gradlew dokka` as content **TODO**

Additionally, the project also provides non Gradle related CI files:

- `job_release.yml`: runs on every pushed tag to automatically create a new
  [release](https://docs.gitlab.com/ee/user/project/releases/). Allows the usage of a template and changelog file to
  construct the release's description **TODO**

## Usage

First, include the template (and its dependencies):

```yaml
include:
  - project: gofancy/pipeline-templates
    file: "/templates/job_build.yml"
    ref: v0.1.0 # you can use any tag here
```

This adds the `.build` job to the pipeline. By definition, jobs starting with a dot are hidden and do not run. To enable
the build job it needs to be extended:

```yaml
build:
  extends: ".build"
```

## Roadmap

- [ ] create all job templates as described above
- [ ] create a deployment pipeline for this project and publish all template files as artifacts (this allows to target a
      specific version of the file)
  - the pipeline should test the project (use the `/ci/lint` endpoint,
    [example of usage in the vcs plugin](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/src/gitlab/gitlab_service.ts#L625))
- [ ] autogenerate an `all.yml` and `full-setup.yml` file on release
